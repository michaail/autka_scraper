const mongoose = require('mongoose');

const { Schema } = mongoose;

const lotSchema = new Schema({
  lotID: String,
  fullName: String,
  make: String,
  model: String,
  year: String,
  startPrice: String,
  price: String,
  plPrice: String,
  uaPrice: String,
  bidsCount: Number,
  buyer: String,
  saleType: String,
  location: String,
  lotInfoHTML: String,
  certificates: [],
  details: {},
}, {
  timestamps: true,
});

module.exports = mongoose.model('Lot', lotSchema);
