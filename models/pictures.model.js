const mongoose = require('mongoose');

const { Schema } = mongoose;

const pictureSchema = new Schema({
  lotId: String,
  links: [],
}, {
  timestamps: true,
});

module.exports = mongoose.model('Pictures', pictureSchema);
