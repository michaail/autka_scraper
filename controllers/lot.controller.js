/* eslint-disable no-console */
const Lot = require('../models/lot.model-mongo');
const utils = require('./utils');


exports.create = async (lotObject) => {
  if (lotObject.lotInfoDescArray === undefined) {
    return;
  }

  const lot = new Lot({
    lotID: lotObject.lotInfoDescArray[2],
    fullName: lotObject.lotInfoDescArray[0],
    year: utils.getFromArray(lotObject.lotInfoDescArray, 0, 0),
    make: utils.getFromArray(lotObject.lotInfoDescArray, 0, 1),
    model: utils.getFromArray(lotObject.lotInfoDescArray, 0, 2),
    location: lotObject.location,
    startPrice: lotObject.startPrice,
    price: lotObject.soldPrice,
    plPrice: lotObject.plPrice,
    uaPrice: lotObject.uaPrice,
    bidsCount: lotObject.bidsCount,
    buyer: lotObject.buyer,
    saleType: lotObject.lotInfoDescArray[5],
    // lotInfoHTML: lotObject.lotInfoHTML,
    certificates: utils.getCertificates(lotObject.lotInfoDescHTML),
    details: lotObject.details,
  });

  lot.save()
    .then(() => {
      console.log(`saved lot: ${lot.lotID}`);
      return new Promise(resolve => resolve());
    }).catch((err) => {
      console.log(`smth went wrong with save: ${err}`);
    });
};
