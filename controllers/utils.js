function getFromArray(array, pos1, pos2) {
  return array[pos1].split(' ')[pos2];
}

// TODO
function getCertificates(lotInfoHTML) {
  const certificates = [];

  if (lotInfoHTML.includes('icon-CERT-E')) {
    certificates.push('CERT-E');
  }
  if (lotInfoHTML.includes('icon-CERT-D')) {
    certificates.push('CERT-D');
  }
  if (lotInfoHTML.includes('icon-CERT-S')) {
    certificates.push('CERT-S');
  }
  if (lotInfoHTML.includes('icon-ADDL-INFO')) {
    certificates.push('ADDL');
  }
  if (lotInfoHTML.includes('icon-FEATURED')) {
    certificates.push('FEATURED');
  }

  return certificates;
}

module.exports.getCertificates = getCertificates;
module.exports.getFromArray = getFromArray;
