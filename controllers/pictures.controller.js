/* eslint-disable no-console */
const Pictures = require('../models/pictures.model');


exports.create = async (picturesData) => {
  const { lotId, links } = picturesData;

  const pictures = new Pictures({
    lotId,
    links,
  });

  pictures.save()
    .then(() => {
      console.log(`saved pictures for: ${lotId}`);
      return new Promise(resolve => resolve());
    }).catch((err) => {
      console.log(`smth went wrong with pic for: ${lotId} \nerror: ${err}`);
    });
};
