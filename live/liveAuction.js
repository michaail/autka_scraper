/* eslint-disable no-console */
/* eslint-disable no-await-in-loop */
const puppeteer = require('puppeteer');

const parser = require('./parser');
const lot = require('../controllers/lot.controller');
const utils = require('./utils');


// async sleep
const sleep = miliSeconds => new Promise(resolve => setTimeout(resolve, miliSeconds));


const appContainerSelector = '#app-container';
const lotInfoDescSelector = '.lotInfoDesc';
const lotDetailsSelector = 'div[ng-class="{\'lotDetailsExpandedTable\': auction.isExpandedMacro}"]';


// get live auction data in real time
async function getLiveAuctionData(dashboardCountry, laneInfo) {
  console.log(`scraping: ${dashboardCountry}`);

  const bidTimerSelector = utils.getBidTimerSelector(dashboardCountry, laneInfo);
  const dashboardURL = utils.getDashboardURL(dashboardCountry, laneInfo);


  const browser = await puppeteer.launch({
    headless: true,
    args: ['--disable-gpu', '--no-sandbox', '--single-process',
      '--disable-web-security', '--disable-dev-profile'],
  });

  const pages = await browser.pages();
  const auctionPage = pages[0];

  await auctionPage.goto(dashboardURL);
  if (dashboardCountry === 'ca' || dashboardCountry === 'com') {
    await auctionPage.reload();
  }


  await auctionPage.waitFor(appContainerSelector).then(() => {
    console.log('app container');
  });

  await auctionPage.waitFor(lotInfoDescSelector).then(() => {
    console.log('lot info');
  });

  await auctionPage.waitFor(5000);


  let hasEnded = false; // indicates that auction expired
  let startPrice = '';
  let price = '';
  let pricePL = '';
  let priceUA = '';
  let buyer = '';
  let bidsCounter = 0;
  let endCounter = 0;
  let previousLotID = '';
  let lotInfoDesc = {};
  const maxTries = 3000; // maximum value of endCounter to end an auction


  console.log('live scraping...');


  while (!hasEnded) {
    let currentLotID;

    try {
      currentLotID = await auctionPage.$eval(lotInfoDescSelector,
        lotInfo => lotInfo.textContent.trim()
          .replace(/[\n\r]+|[\s]{2,}/g, ';')
          .split(';')
          .filter(value => value !== '')[2]);
    } catch (error) {
      // TODO in future handle retry
    }


    if (currentLotID !== previousLotID) {
      endCounter = 0;

      if (price) {
        // console.log(`sold for: ${price}`);
        lotInfoDesc.startPrice = startPrice;
        lotInfoDesc.soldPrice = price;
        lotInfoDesc.buyer = buyer;
        lotInfoDesc.location = laneInfo.url.auctionID;
        lotInfoDesc.plPrice = pricePL;
        lotInfoDesc.uaPrice = priceUA;
        lotInfoDesc.bidsCount = bidsCounter;

        await lot.create(lotInfoDesc);
      }

      previousLotID = currentLotID;


      buyer = '';
      price = '';
      pricePL = '';
      priceUA = '';
      bidsCounter = 0;


      lotInfoDesc = await auctionPage.$eval('.lotInfoDesc', (lotInfo) => {
        const lotInfoDescArray = lotInfo.textContent.trim()
          .replace(/[\n\r]+|[\s]{2,}/g, ';')
          .split(';')
          .filter(value => value !== '');

        const lotInfoDescHTML = lotInfo.innerHTML.trim();

        return {
          lotInfoDescArray,
          lotInfoDescHTML,
        };
      });


      const lotInfoDetailsArray = await auctionPage.$eval(lotDetailsSelector, (lotDetails) => {
        const text = lotDetails.textContent.trim()
          .replace(/[\n\r]+|[\s]{2,}/g, ';')
          .split(';')
          .filter(value => value !== '');

        return text;
      });


      lotInfoDesc.details = parser.parseLotDetails(lotInfoDetailsArray);
    } else {
      const scrapedData = await auctionPage.$eval(bidTimerSelector, (bidTimer) => {
        const result = {};

        let timerContent = '';

        try {
          timerContent = bidTimer.querySelector('svg').textContent;
        } catch (error) {
          // TODO retry feature
        }


        if (timerContent !== undefined) {
          try {
            result.price = timerContent.match(/\$((?:\d|,)*\.?\d+)/g)[0] || null;
          } catch (err) {
            console.log(`error obtaining price ${err}`);
          }
        } else {
          return {
            price: null,
            bidder: null,
          };
        }


        if (timerContent.includes('Poland')) {
          result.bidder = 'PL';
        } else if (timerContent.includes('Ukraine')) {
          result.bidder = 'UA';
        } else if (timerContent.includes('Ontario')) {
          result.bidder = 'ON';
        } else if (timerContent.includes('Quebec')) {
          result.bidder = 'QC';
        } else if (timerContent.includes('Alberta')) {
          result.bidder = 'AB';
        } else {
          result.bidder = null;
        }

        return result; // [price, price, bidderInfo]
      });
      if (price !== scrapedData.price && scrapedData.price !== null) {
        ({ price } = scrapedData);
        console.log(price);

        if (bidsCounter === 0) {
          startPrice = price;
        }
        bidsCounter += 1;
        endCounter = 0;


        if (Object.keys(scrapedData).length === 2) {
          if (scrapedData.bidder === 'PL') {
            buyer = 'PL';
            pricePL = price;
          } else if (scrapedData.bidder === 'UA') {
            buyer = 'UA';
            priceUA = price;
          } else {
            buyer = scrapedData.bidder;
          }
        }
      }


      // Last Lot handler
      if (endCounter >= maxTries) {
        if (scrapedData.price !== null) {
          console.log(`sold last lot for: ${price}`);

          lotInfoDesc.startPrice = startPrice;
          lotInfoDesc.soldPrice = price;
          lotInfoDesc.buyer = buyer;
          lotInfoDesc.location = laneInfo.url.auctionID;
          lotInfoDesc.plPrice = pricePL;
          lotInfoDesc.uaPrice = priceUA;
          lotInfoDesc.bidsCount = bidsCounter;

          lot.create(lotInfoDesc);
          hasEnded = true;

          await browser.close();
        } else {
          endCounter = 0;
        }
      }

      endCounter += 1;

      await sleep(300);
    }
  }

  return true;
}


module.exports.getLiveAuctionData = getLiveAuctionData;
