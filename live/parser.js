const fields = ['Location:', 'Doc Type:', 'Odometer:',
  'Highlights:', 'Primary Damage:', 'Secondary Damage:',
  'Est. Retail Value:', 'VIN:', 'Body Style:',
  'Color:', 'Engine Type:', 'Cylinders:',
  'Drive:', 'Fuel:', 'Keys:',
];

exports.parseLotDetails = (detailsTable) => {
  const lotDetailsObj = {};
  if (detailsTable == null) {
    return null;
  }

  fields.forEach((field) => {
    if (detailsTable.includes(field)) {
      lotDetailsObj[field.slice(0, -1).replace('.', '')] = detailsTable[detailsTable.indexOf(field) + 1];
    }
  });

  return lotDetailsObj;
};
