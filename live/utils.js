// generates bidTimerSelector for given auction/lane details
exports.getBidTimerSelector = (dashboardCountry, laneInfo) => {
  // bidTimerContainer-COPART023A result example

  const prefix = '#bidTimerContainer-COPART';
  let suffix = ''; // empty by default
  let completion0 = '';

  if (dashboardCountry === 'co.uk') {
    suffix = 'UK';
  }
  if (laneInfo.url.auctionID.length === 2) {
    completion0 = '0';
  } else if (laneInfo.url.auctionID.length === 1) {
    completion0 = '00';
  }

  return prefix + suffix + completion0 + laneInfo.url.auctionID + laneInfo.url.lane;
};


// generates dashboardURL for given auction/lane details
exports.getDashboardURL = (dashboardCountry, laneInfo) => {
  // https://auction.copart.com/c3/auctions.html?siteLanguage=en&appId=g2#201-B result example
  const prefix = `https://auction.copart.${dashboardCountry}`;
  if (dashboardCountry === 'ca') {
    return `${prefix}/c3/auctions.html?site=ca&appId=G2&siteLanguage=en-CA&appId=g2#${
      laneInfo.url.auctionID}-${laneInfo.url.lane}`;
  }
  if (dashboardCountry === 'com') {
    return `${prefix}/c3/auctions.html?appId=G2&siteLanguage=en&appId=g2#${
      laneInfo.url.auctionID}-${laneInfo.url.lane}`;
  }
  if (dashboardCountry === 'co.uk') {
    return `${prefix}/c3/auctions.html?siteLanguage=en&appId=g2#${
      laneInfo.url.auctionID}-${laneInfo.url.lane}`;
  }
  return null;
};
