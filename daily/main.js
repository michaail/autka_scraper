/*
  Status: working no major issues
  Usage: call from dailyScheduler.js with country param
  Author: Michał Kłos
*/

/* eslint-disable no-console */
/* eslint-disable no-constant-condition */
/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable camelcase */

const puppeteer = require('puppeteer');

const fs = require('fs');
const serializer = require('./serializer.js');


const laterTodayAuctionsSelector = 'tr[ng-repeat="auction in list = (data.laterTodaysAuctions)"]';
const dataTablesSelector = 'td[class="dataTablesEmpty"]';
const appLabelViewListSelector = 'a[ng-bind="locale.messages[\'app.label.viewList\']"]';


// get all later today auction
const getTodayAuctionsURLs = async (dashboardCountry) => {
  console.log(`scraping: ${dashboardCountry}`);

  const dashboardURL = `https://www.copart.${dashboardCountry}/todaysAuction/`;


  // run browser headless
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--disable-gpu', '--no-sandbox', '--single-process',
      '--disable-web-security', '--disable-dev-profile'],
  });

  const pages = await browser.pages();
  const dashboardPage = pages[0];


  await dashboardPage.goto(dashboardURL);


  let count = 0;
  const retry = 2;
  // await for table of todays auctions with retry feature
  while (true) {
    try {
      // wait for table with todaysAuctions
      await dashboardPage.waitFor(laterTodayAuctionsSelector);

      break;
    } catch (error) {
      if (++count === retry) { // reached maximal retry count
        const dataTablesEmpty = await dashboardPage.$eval(dataTablesSelector,
          text => text.textContent);

        if (dataTablesEmpty === 'There are no auctions available at this time.') {
          console.log(`No auctions today for: ${dashboardCountry}`);
          await browser.close();

          return { // empty tables
            todayAuctionsTable: [],
            linksPrimitives: [],
          };
        }
        // TODO logger
        console.log(`can't reach page: ${dashboardURL} error: reached maximal retry count`);
        await browser.close();

        return null;
      }
      // TODO logger
      console.log(`wait for reload...\nerror: ${error}`);
      await dashboardPage.reload();
    }
  }


  const todayAuctionsTable = await dashboardPage.$$eval(laterTodayAuctionsSelector, (table) => {
    const result = [];

    table.forEach((row) => {
      const content = row.textContent.trim()
        .replace(/[\n\r]+|[\s]{2,}/g, ';')
        .split(';;'); // clears all whitespaces and makes to an array

      result.push(content);
    });

    return result;
  });


  const linksPrimitives = await dashboardPage.$$eval(appLabelViewListSelector, (table) => {
    const result = [];

    table.forEach((row) => {
      const url = row.getAttribute('data-url');

      if (url.includes('/saleListResult/') && url.includes('?')) {
        const splitUrl = url.slice(0, url.indexOf('?'))
          .replace('/saleListResult/', '')
          .split('/'); // clears all unnecessary stuff from URL

        splitUrl.splice(1, 1);
        result.push(splitUrl);
      }
    });

    return result;
  });

  await browser.close();
  return {
    todayAuctionsTable,
    linksPrimitives,
  };
};


// fn callback returns a Promise when file saved successfully and reject otherwise
exports.getTodayAuctionsURLsHandler = (country, fn) => {
  getTodayAuctionsURLs(country).then((result) => {
    if (result == null) {
      console.log('received null -> exiting');
      return;
    }


    const resultArray = [];
    for (let i = 0; i < result.todayAuctionsTable.length; i++) {
      const auctionRow = result.todayAuctionsTable[i];
      const linkRow = result.linksPrimitives[i];

      if (linkRow[0] === '201' || linkRow[0] === '202' || linkRow[0] === '205' || linkRow[0] === '206' || linkRow[0] === '207' || linkRow[0] === '295') {
        serializer.serializeLaterTodayTableToJSON(resultArray, auctionRow, linkRow);
      }
    }


    const todayAuctionsObject = {
      date: new Date(),
      country,
      laterToday: resultArray,
    };


    // write JSON data to file @ /todayAuctions/[country].json
    fs.writeFile(`./todayAuctions/${country}.json`, JSON.stringify(todayAuctionsObject, null, 4), (err) => {
      fn(new Promise((resolve, reject) => {
        if (err) {
          reject(err);
        }

        console.log(`Saved for${country}`);
        resolve();
      }));
    });
  });
};
