function getTimeObject(timeString) {
  const timeSplit = timeString.split(' ');

  return {
    hour: timeSplit[0],
    meridiem: timeSplit[1],
    timeZone: timeSplit[2],
  };
}

function getLinkObject(linkArray) {
  return {
    auctionID: linkArray[0],
    lane: linkArray[1],
  };
}


exports.serializeLaterTodayTableToJSON = (result, content, link) => {
  switch (content.length) {
    case 8:
      result.push({
        time: getTimeObject(content[0]),
        name: content[1],
        region: content[3],
        lanes: [{
          type: content[4],
          lane: content[5],
          items: content[6],
          url: getLinkObject(link),
        }],
      });
      break;

    case 4:
      result[result.length - 1].lanes.push({
        type: content[0],
        lane: content[1],
        items: content[2],
        url: getLinkObject(link),
      });
      break;

    case 10:
      result.push({
        time: getTimeObject(content[0]),
        name: content[1],
        region: content[3],
        lanes: [{
          type: content[4],
          lane: content[7],
          items: content[8],
          url: getLinkObject(link),
        }],
      });
      break;

    case 6:
      result[result.length - 1].lanes.push({
        type: content[0],
        lane: content[3],
        items: content[4],
        url: getLinkObject(link),
      });
      break;

    default:
      break;
  }
};
