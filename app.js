/* eslint-disable no-console */
const schedule = require('node-schedule');
const daily = require('./daily/main');
const liveScheduler = require('./liveScheduler');

const countryArray = ['ca'];


schedule.scheduleJob('55 8 * * 1,2,3,4,5', () => {
  console.log(`get today auctions: ${new Date()}`);

  countryArray.forEach((country) => {
    daily.getTodayAuctionsURLsHandler(country, (promise) => {
      promise.then(() => {
        console.log(true);

        liveScheduler.scheduleAuctions(country);
      }).catch((err) => {
        console.log(err);
      });
    });
  });
});
