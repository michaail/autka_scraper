const axios = require('axios');

async function fetchLotsPage(page, pageSize) {
  const recvData = await axios.get(`http://localhost:3001/api/lots?page=${page}`);

  return {
    documents: recvData.data.documents,
    meta: recvData.data.meta,
  };
}

module.exports.fetchLotsPage = fetchLotsPage;
