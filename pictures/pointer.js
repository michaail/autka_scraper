const fs = require('fs');

function setPointer(country, pointer) {
  fs.writeFileSync(`./data/${country}.json`, JSON.stringify({ pointer }, null, 4));
}

function getPointer(country) {
  const buff = fs.readFileSync(`./data/${country}.json`);
  const { pointer } = JSON.parse(buff);
  return pointer;
}

module.exports.setPointer = setPointer;
module.exports.getPointer = getPointer;
