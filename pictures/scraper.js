/* eslint-disable no-console */
// const puppeteer = require('puppeteer');

// const pictures = require('../controllers/pictures.controller');

const sleep = miliSeconds => new Promise(resolve => setTimeout(resolve, miliSeconds));


async function getPicturesData(page, lotID) {
  await page.goto(`http://www.copart.ca/lot/${lotID}`);

  await page.waitFor('.bxslider');
  console.log('reached galleria');

  await page.waitFor(2000);

  const content = await page.$eval('.bxslider', (date) => {
    // const html = date.innerHTML;
    const divs = date.querySelectorAll('li');
    const inners = [];
    divs.forEach((div) => {
      const source = div.querySelector('img');
      inners.push(source.getAttribute('lazy-url'));
    });

    return inners;
  });

  console.log(content);
  return content;
}

module.exports.getPicturesData = getPicturesData;
