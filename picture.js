/* eslint-disable no-await-in-loop */
// statements inside loop are ment to be sequential
const puppeteer = require('puppeteer');

const scraper = require('./pictures/scraper');
const pictures = require('./controllers/pictures.controller');

const req = require('./pictures/requests');
const file = require('./pictures/pointer');

const mongo = require('./mongo/db');

const id = '47237608';

const country = 'ca';
mongo.connect(country);

let t = true;
let counter = 0;


async function main(rounds) {
  const browser = await puppeteer.launch({
    headless: false,
    args: ['--disable-gpu', '--no-sandbox', '--single-process',
      '--disable-web-security', '--disable-dev-profile'],
  });

  const pages = await browser.pages();
  const page = pages[0];

  // performs every page/pointer
  while (t) {
    const pointer = file.getPointer(country);
    let documents;
    let meta;

    try {
      ({ documents, meta } = await req.fetchLotsPage(pointer));
    } catch (error) {
      return;
    }

    for (const lot of documents) {
      const sources = await scraper.getPicturesData(page, lot.lotID);
    }


    pictures.create({
      lotId: id,
      links: sources,
    });

    file.setPointer(country, newPointer);

    // while controller
    counter += 1;
    if (counter === rounds) {
      t = false;
    }
  }

  const d = await req.fetchLotsPage(1);
  console.log(d);


  // const sources = await scraper.getPicturesData(page, id);

  console.log(sources);
}
main();
