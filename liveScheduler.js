/* eslint-disable no-console */
const fs = require('fs');
const scheduler = require('node-schedule');
const convertTime = require('convert-time');

const live = require('./live/liveAuction');
const mongo = require('./mongo/db');


function getAuctionsArray(country) {
  const plainContent = fs.readFileSync(`./todayAuctions/${country}.json`);
  return JSON.parse(plainContent);
}

exports.scheduleAuctions = async (country) => {
  const auctions = getAuctionsArray(country);

  if (auctions.length === 0) {
    console.log(`no auctions today for: ${country}`);
    return;
  }


  let counter = 0;

  const currDate = new Date();
  const currDay = currDate.getDate();
  const currMonth = currDate.getMonth();
  const currYear = currDate.getFullYear();

  auctions.laterToday.forEach((auction) => {
    const fullHour = convertTime(auction.time.hour + auction.time.meridiem);

    const hour = parseInt(fullHour.split(':')[0], 10) - 1;
    const scheduleRule = new Date(currYear, currMonth, currDay, hour, 58, 0);

    auction.lanes.forEach((lane) => {
      counter += 1;
      console.log(`counter: ${counter}`);

      scheduler.scheduleJob(scheduleRule, () => {
        mongo.connect(country);

        live.getLiveAuctionData(country, lane).then(() => {
          console.log(`${lane.url.auctionID} - ${lane.url.lane} : ended`);
          counter -= 1;

          console.log(`counter: ${counter}`);

          if (counter === 0) {
            console.log('all auction ended');
            mongo.close();
          }
        });
      });
    });
  });
};
