/* eslint-disable no-console */
const mongoose = require('mongoose');
const config = require('./dbConfig');


exports.connect = async (country) => {
  let url;
  if (country === 'ca') {
    url = `${config.dbUrl}canada`;
  } else if (country === 'com') {
    url = `${config.dbUrl}us`;
  } else {
    url = `${config.dbUrl}autka-test`;
  }

  if (mongoose.connection.readyState === 0) {
    await mongoose.connect(url, {
      useNewUrlParser: true,
    }).then(() => {
      console.log('connected to db');
    }).catch((err) => {
      console.log(`couldn't connect to db: ${err}`);
      process.exit();
    });
  }
};

exports.close = () => {
  mongoose.connection.close(() => {
    console.log('connection closed');
  });
};
